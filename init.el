(setq user-full-name "Prajwal Prakash")
(setq user-mail-address "prajwalpy6@gmail.com")


;; Package Management using package.el
(require 'package)
(package-initialize)

(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;; package-refresh-contents)

;; Packages to be installed
(defvar package-list '(auto-complete
		       color-theme-sanityinc-tomorrow
		       counsel
		       elixir-mode
		       flyspell
		       lua-mode
		       markdown-mode
		       ruby-end
		       rust-mode
		       web-mode
		       yaml-mode))

(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))


;; Splash Screen
(setq inhibit-splash-screen t
      initial-scratch-message nil
      initial-major-mode 'org-mode)


;; Scroll Bar, Tool bar, Menu Bar
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)


;; Disable autosave
(setq make-backup-files nil)


;; Display Settings
(setq-default indicate-empty-lines t)
(when (not indicate-empty-lines)
  (toggle-indicate-empty-lines))


;; Default Font
(set-default-font "DejaVu Sans Mono 12")
;; Don't display comments in italic
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:slant normal)))))


;; Indentation
(setq tab-width 2
      indent-tabs-mode nil)


;; Back up files
(setq make-backup-files nil)


;; Yes and No
(defalias 'yes-or-no-p 'y-or-n-p)


;; Highlight trailing whitespace
(setq-default show-trailing-whitespace t)


;; Marking Text

;; typed text replaces selection
(delete-selection-mode t)
;; the region is highlighted when it is marked
(transient-mark-mode t)
;; cutting and pasting uses clipboard
(setq select-enable-clipboard t)


;; Key Bindings
(global-set-key (kbd "RET") 'newline-and-indent)
(global-set-key (kbd "C-;") 'comment-or-uncomment-region)


;; 80 character wrap
(setq-default fill-column 80)


;; Display current line in the status bar
(line-number-mode t)


;; Set a padding on the left of text
(set-window-margins nil 1)


;; Auto Complete
(require 'auto-complete-config)
(ac-config-default)
(ac-linum-workaround)


;; Theme
(load-theme 'sanityinc-tomorrow-bright t)


;; Ivy Mode
(ivy-mode 1)
(setq ivy-use-virtual-buffers t)
(setq ivy-count-format "(%d/%d) ")

;; Councel, Ivy and Swiper key bindings
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)

;; Flyspell Dictionary
(setq ispell-program-name "aspell")
;; Please note ispell-extra-args contains ACTUAL parameters passed to aspell
(setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))


;; Major modes

;; Ruby Mode
(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.gemspec$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.ru$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Rakefile" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile" . ruby-mode))
(add-to-list 'auto-mode-alist '("Vagrantfile" . ruby-mode))
(add-hook 'ruby-mode-hook
	  (lambda()
	    '(ruby-end-mode t)
	    '(electric-pair-mode t)))


;; Python Mode

(eval-after-load "python-mode"
  '(add-hook 'python-mode-hook 'electric-pair-mode))

;; Web Mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(setq web-mode-markup-indent-offset 2)
(setq web-mode-css-indent-offset 2)
(setq web-mode-code-indent-offset 2)
(setq web-mode-enable-auto-closing t)
(setq web-mode-enable-auto-pairing t)


;; JavaScript Mode
(setq js-indent-level 2)


;; CoffeeScript Mode
(custom-set-variables '(coffee-tab-width 2))


;; Markdown Mode
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.mdown$" . markdown-mode))
(add-hook 'markdown-mode-hook
	  (lambda ()
	    (visual-line-mode t)
	    (writegood-mode t)
	    (flyspell-mode t)))
